import React, { Component } from 'react';
import MovieFilters from './components/MovieFilters/MovieFilters';
import MovieList from './components/MovieList/MovieList';
import axios from 'axios';

import classes from './App.module.css';
class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      movies : [],
      filtered_movies: [],
      img_info : {
        url : '',
        size: ''
      },
      current_genres_id:[],
      genres:[],
      selected_genres:[],
      current_rating: 3,
      loaded: false
    }
  }

  componentDidMount () {

    //get img info from the api and store in state
    axios.get('https://api.themoviedb.org/3/configuration?api_key=479b4bf6ad9248e77c4308ae0d924649')
    .then(res => {
        const imageinfo = res.data.images
        this.setState({ 
          img_info : {
            url: imageinfo.secure_base_url,
            size: imageinfo.poster_sizes[4]
          } 
      });
    })
    //get genre info first and store in state
    axios.get('https://api.themoviedb.org/3/genre/movie/list?api_key=479b4bf6ad9248e77c4308ae0d924649&language=en-US')
    .then(res => {
      const genres = res.data.genres.map(item => item);
      this.setState({ 
        genres
      });

      //now grab movies info and then store in state using only properties that are required
      return axios.get('https://api.themoviedb.org/3/movie/now_playing?api_key=479b4bf6ad9248e77c4308ae0d924649&language=en-US&page=1')
    })
    .then(res => {
      // generate array that all the genre ids will get pushed into
      const genres_ids = [];
      const movies = res.data.results.map(item => {
        genres_ids.push(...item.genre_ids)
        return {
          popularity: item.popularity,
          poster_path: item.poster_path,
          id: item.id,
          title: item.title,
          genre_details: this.getGenreDetails(item.genre_ids).sort(this.orderByName),
          vote_average: item.vote_average
        };
      }
      );
      //remove any duplicates from the genres using Set
      this.setState({ 
        movies,
        filtered_movies: movies,
        current_genres_id : this.getGenreDetails([...new Set(genres_ids)]),
        loaded: true
      },this.updateMovieListings); 
    })
    
  }

  /**
   * Pass in the genre ids to gets the genre name from the master genre array in state .
   *
   * @param genres_ids array
   * 
   * @returns {array}
   */
  getGenreDetails = (genres_ids) => {
      const genres = [...this.state.genres];
      const hydrateGenre = genres_ids.map(id => {
        return genres.find(item => {
             return item.id === id
        })
      })
      return hydrateGenre;
  }

  /**
   * order function to help order the results by the popularity property
   */
  orderByPopularity = ( a, b ) => {
    if ( a.popularity < b.popularity ){
      return 1;
    }
    if ( a.popularity > b.popularity ){
      return -1;
    }
    return 0;
  }

  /**
   * order function to help order the genre name by the alphabetical order
   */
  orderByName = ( a ,b ) => {
    if(a.name < b.name) { 
      return -1; 
    }
    if(a.name > b.name) { 
      return 1; 
    }
    return 0;
  }

  /**
   * 
   * Handles the onchange event of the genre checkboxes. Checking if the checkbox is checked or not to update the selected_genres array in state. After setting state then need to call the updateMovieListings function
   * 
   * @param event
   * @param id number
   */
  onChangeGenreHandler = (event,id) => {

    //create a copy
    let selectedArr = [...this.state.selected_genres];

    if(!event.target.checked){
      selectedArr.splice(selectedArr.indexOf(id),1)
    }else{
      selectedArr.push(id)
    }

    this.setState({
      selected_genres: selectedArr
    }, this.updateMovieListings )

  }

  /**
   * 
   * Handles the onchange event of the rating slider. Updates the current_rating number in state. After setting state then need to call the updateMovieListings function
   * 
   * @param event
   */
  onChangeRatingHandler = (event) => {
    this.setState({
      current_rating: event.target.value 
    }, this.updateMovieListings )
  }

  /**
   * 
   * Function to filter the movies based on both input updates. Have a full copy of the movies from state and then apply a series of filters to get the desired results.
   * 
   */  
  updateMovieListings = () => {
    //take a copy of the movies
    let newList = [...this.state.movies];

    //loop through the current selected_genres in state and filter listings to ensure only movies with the multiple genres are shown
    for (let arrItem of this.state.selected_genres){
      newList = newList.filter(item => {
          return item.genre_details.find(item => {
            return item.id === arrItem
          })
      })
    }

    //filter out any movies below the current rating
    newList = newList.filter(item => {
        return item.vote_average >= this.state.current_rating
    })
    
    this.setState({
      filtered_movies: newList 
    })

  }
  
  render() {
    return (
        <div className={classes.container}>
          <MovieFilters 
          genres={this.state.current_genres_id.sort(this.orderByName)} 
          updateGenre={this.onChangeGenreHandler} 
          updateRating={this.onChangeRatingHandler}
          rating={this.state.current_rating}/>
          <MovieList 
          movies={this.state.filtered_movies.sort(this.orderByPopularity)} 
          imgInfo={this.state.img_info}
          loaded ={this.state.loaded}/>
        </div>
    )
  }
}

export default App;
