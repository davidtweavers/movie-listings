import React from 'react';

import classes from './MovieRatings.module.css';

const movieRatings = (props) => {
  return (
    <div>
      <input onChange={(event) => props.updateRating(event)} defaultValue ="3" type="range" min="0" max="10" step="0.5" />
      <span className={classes.RatingNumber}>{props.rating}</span>
    </div>
  )
}

export default movieRatings;