import React from 'react';
import classes from './MovieGenres.module.css';

const movieGenres = (props) => {
  return (
    <form className={classes.FilterGenre}> 
      {props.genres.map((item) =>{
        return (
          <span key={item.id} className={classes.FilterGenreItem}>
            <input 
            type="checkbox" 
            id={item.name} 
            value={item.id} 
            name={item.name} 
            onChange={(event) => props.updateGenre(event,item.id)}/>
            <label htmlFor={item.name}>
              {item.name}
            </label>
          </span>
        )
      })}
    </form>
  )
}

export default movieGenres;