import React from 'react';
import MovieGenres from './MovieGenres/MovieGenres';
import MovieRatings from './MovieRatings/MovieRatings';

import classes from './MovieFilters.module.css';

const movieFilters = (props) => {
  return (
    <div className={classes.Filters}>
      <div className={classes.FiltersTitle}>Genres:</div>
      <MovieGenres genres={props.genres} updateGenre={props.updateGenre}/>
      <div className={classes.FiltersTitle}>Rating:</div>
      <MovieRatings rating={props.rating} updateRating={props.updateRating}/>
    </div>
  )
}

export default movieFilters;