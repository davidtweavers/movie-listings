import React from 'react';
import MovieListItem from './MovieListItem/MovieListItem';

import classes from './MovieList.module.css';

const movieList = (props) => {

  return (
    props.movies.length ? 
    <ul className={classes.MovieList}>
      {props.movies.map(item => (
        <MovieListItem 
        key={item.id}
        title={item.title}
        imgUrl={props.imgInfo.url}
        imgSize={props.imgInfo.size}
        imgPath={item.poster_path}
        genres={item.genre_details}
        popularity={item.popularity}
        vote_average={item.vote_average}
        />
      )
    )}
    </ul> 
    : 
    //this makes sure the 'No matches' text doesn't show on page load only when the filtered movie array is empty
    props.loaded ? <p>No matches</p> : null
  )
}

export default movieList;