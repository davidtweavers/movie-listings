import React from 'react';

import classes from './MovieListItem.module.css';

const movieListItem = (props) => {

  return (
    <li className={classes.MovieListItem}>
      <div className={classes.MovieListItemTitle}>{props.title}</div>
      <div className={classes.MovieListItemGenres}>Genres: {props.genres.map((item,index) => {
        return (index ? ', ': '') + item.name
      })}
      </div>
      <img alt={props.title} src={props.imgUrl + '/'+props.imgSize+'/'+props.imgPath} />
    </li>
  )
}

export default movieListItem;

