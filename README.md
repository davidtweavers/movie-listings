This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you should run:

* `yarn install`
* `yarn start`

This runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

## Styling

Picked a cool Google font and used CSS Modules to provide some simple and clean styling.

## Thought process

Used axios to initially retrieve the api info needed for the images and picked a size. Then needed to get the genre information from that api which could then be stored before getting the 'Now Playing' movie api information. Needed to stagger this so I could get hydrate the genre info using the genre id to get the genre name and finalize the movie objects for state. Only used properties that need to be used from the 'Now Playing' api. The input apis should only be called once.

Used the App.js as the file to control the state and pass the info down to the different functional components. 

The movies are sorted to be ordered by the popularity property and made sure to order the genres alphabetically too.

The inputs for the genre checkboxes and the ratings range are used to call a function onchange that updates a filtered_movies property in state. That function creates a copy of the untouched movie property state and then loops through an array in state for the currently selected genres to make sure the movies are filtered by each selected genre id and only movies with those genres are shown. The filtered_movies array is then filtered again by the vote_average property.

Added a ternary operator in the MovieList component to add some text if no movies match the filters.